package bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class QuizBot {

    private static final Logger logger = LoggerFactory.getLogger(QuizBot.class);

    private static final String QUIZ_PROPERTIES = "quiz.properties";
    private static final String SLEEP_SECONDS = "sleep_seconds";
    private static final String DEFAULT_MESSAGE = "default_message";

    private static Properties quizProperties;
    private static Long sleepMillis;
    private final Map<Long, Set<String>> chatIdToCountQuestions;
    private final Map<Long, Long> chatIdToLastMessageTime;
    private final Map<Long, Long> chatIdToCnt;

    public QuizBot() {
        quizProperties = new Properties();
        try {
            quizProperties.load(new InputStreamReader(new FileInputStream(QUIZ_PROPERTIES), StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.error("Error", e);
            throw new RuntimeException("error read properties");
        }

        String sleepProperty = quizProperties.getProperty(SLEEP_SECONDS);
        sleepMillis = Long.parseLong(sleepProperty != null ? sleepProperty : "5") * 1000;
        chatIdToCountQuestions = new HashMap<>();
        chatIdToLastMessageTime = new HashMap<>();
        chatIdToCnt = new HashMap<>();
    }

    public String[] getAnswers(String inputText) {
        final String answers = quizProperties.getProperty(inputText);
        return answers != null ? answers.split(";") : null;
    }

    public boolean isMustAnswer(Long chatId) {
        return chatIdToCountQuestions.getOrDefault(chatId, new HashSet<>()).size() < 2;
    }

    public double getLatMessageTimeByChatId(Long chatId) {
        return chatIdToLastMessageTime.getOrDefault(chatId, 0L);
    }

    public long getSleepMillis() {
        return sleepMillis;
    }

    public String getDefaultMessage() {
        final String defaultMessage = quizProperties.getProperty(DEFAULT_MESSAGE);
        return defaultMessage != null ? defaultMessage : "Ooops...";
    }

    public void addQuestionToChatId(Long chatId, String inputText) {
        Set<String> inputTexts = chatIdToCountQuestions.getOrDefault(chatId, new HashSet<>());
        inputTexts.add(inputText);
        chatIdToCountQuestions.put(chatId, inputTexts);

    }

    public void addLastMessageTimeToChatId(Long chatId, long currentTimeMillis) {
        chatIdToLastMessageTime.put(chatId, currentTimeMillis);

    }

    public long getFirstSleepMillis(Long chatId, long currentTimeMillis) {
        if (currentTimeMillis - getLatMessageTimeByChatId(chatId) < getSleepMillis()) {
            return getSleepMillis() * chatIdToCnt.getOrDefault(chatId, 2L);
        }
        return 0;
    }

    public void addCntToChatId(Long chatId) {
        chatIdToCnt.put(chatId, 1L);
    }
}
