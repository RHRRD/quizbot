package bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Main extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String BOT_PROPERTIES = "bot.properties";

    private static String BOT_USERNAME;
    private static String BOT_TOKEN;

    private final QuizBot quizBot;

    private Main(DefaultBotOptions options) {
        super(options);
        Properties botProperties = new Properties();
        try {
            botProperties.load(new InputStreamReader(new FileInputStream(BOT_PROPERTIES), StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.error("Error", e);
            throw new RuntimeException("error read properties");
        }
        BOT_USERNAME = botProperties.getProperty("BOT_USERNAME");
        BOT_TOKEN = botProperties.getProperty("BOT_TOKEN");
        quizBot = new QuizBot();
    }

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi bot = new TelegramBotsApi();

        DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);

        Main main = new Main(botOptions);
        try {
            bot.registerBot(main);
            logger.info("Bot start successfully!");
        } catch (TelegramApiException ex) {
            logger.error("Error during register bot", ex);
        }
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            Message inputMessage = update.getMessage();
            SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId());
            sendMessage.setParseMode("Markdown");
            String inputUpperText = getInputUpperText(inputMessage);

            final String[] quizAnswers = quizBot.getAnswers(inputUpperText);
            Long chatId = inputMessage.getChatId();
            if (quizAnswers != null && quizBot.isMustAnswer(chatId)) {
                sleep(quizBot.getFirstSleepMillis(chatId, System.currentTimeMillis()));
                quizBot.addQuestionToChatId(chatId, inputUpperText);
                sendMessage.setText("_" + inputUpperText.replace('_', ' ') + "_: " + quizAnswers[0]);
                sendMessage(sendMessage);
                quizBot.addLastMessageTimeToChatId(chatId, System.currentTimeMillis());
                sleep(quizBot.getSleepMillis());
                if (quizAnswers.length > 1) {
                    quizBot.addCntToChatId(chatId);
                    sendMessage.setText("_" + inputUpperText.replace('_', ' ') + "_: " + quizAnswers[1]);
                    sendMessage(sendMessage);
                    quizBot.addLastMessageTimeToChatId(chatId, System.currentTimeMillis());
                }
            } else {
                sendMessage.setText(quizBot.getDefaultMessage());
                sendMessage(sendMessage);
            }
        }
    }

    private String getInputUpperText(Message inputMessage) {
        String inputMessageText = inputMessage.getText().replace(' ', '_');
        return inputMessageText.toUpperCase();
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            logger.error("InterruptedException: ", e);
        }
    }

    private void sendMessage(SendMessage sendMessage) {
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            logger.error("Error during sendMessage", e);
        }
    }

    @Override
    public String getBotUsername() {
        return BOT_USERNAME;
    }

}
